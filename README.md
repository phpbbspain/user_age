# phpBB Display Age Users
A phpBB 3.1 extension that adds a Scroll to Up and Bottom button making it easy to return to the up and bottom of a page.

## Extensión validada oficialmente por phpBB 
[Official Topic - phpBB Display Age Users](https://www.phpbb.com/community/viewtopic.php?t=2411981)

[Official Support - phpBB Display Age Users](https://www.phpbb.com/customise/db/extension/phpbb_display_age_users/support)

## Corrección de errores
Para cualquier cambio a realizar, simplemente editar para realizar el cambio y "Pull Request". 

## License
[GNU General Public License v2](http://opensource.org/licenses/GPL-2.0)

## Autor
ThE KuKa (Raúl Arroyo Monzo)

## © 2003 / 2017 [phpBB España](http://www.phpbb-es.com)

![phpBB Spain](http://www.phpbb-es.com/images/logo_es.png)